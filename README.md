# README

This applications is a proof of concept project used to demonstrate using Stimulus JS in a Ruby on RAils project.

It is mostly developed using this tutorial: https://www.digitalocean.com/community/tutorials/how-to-add-stimulus-to-a-ruby-on-rails-application

In order to run correctly, make sure that yarn is installed locally, then

run bundle exec rails webpacker:install to install webpacker, and then
run bundle exec rails webpacker:install:stimulus to install stimulus.
